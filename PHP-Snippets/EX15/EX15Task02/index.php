<?php

    $firstName = "John";
    $lastName = "Doe";

    //This will not show the values of the variables - the variables are local to the function only.
    function noGlobal1()
    {
        echo $firstName."<br>";
        echo $lastName."<br>";
    }

    noGlobal1();
    echo "<br><br>";

    //Option 1: Using Parameters
    function noGlobal2($fn, $ln)
    {
        echo $fn."<br>";
        echo $ln."<br>";
    }

    noGlobal2($firstName, $lastName);
    echo "<br><br>";

    //Option 2: Using Global Associative Array
    function sgvGlobal()
    {
        //Looks at the variables in the global scope
        echo $GLOBALS['firstName']."<br>";
        echo $GLOBALS['lastName']."<br>";
    }

    sgvGlobal();
    echo "<br><br>";

    //Option 3: Using global keyword
    function usingGlobal()
    {
        global $firstName;
        global $lastName;

        echo $firstName."<br>";
        echo $lastName."<br>";
    }

    usingGlobal();
    echo "<br><br>";

?>