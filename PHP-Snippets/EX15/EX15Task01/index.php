<?php

    //Watch where the echo is, to control what is printed to the screen.

    function aMessage1() 
    {
        echo "Functions can just print something"."<br>";
    }

    aMessage1();

    echo "<br><br>";

    function aMessage2($message) 
    {
        echo $message."<br>";
    }

    aMessage2("This can be a string");
    aMessage2(123456789);

    echo "<br><br>";

    function aMessage3($message) 
    {
        return $message;
    }

    echo aMessage3("This can be a string")."<br>";
    echo aMessage3(123456789)."<br>";

?>