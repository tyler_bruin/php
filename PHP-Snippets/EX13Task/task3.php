<?php

    $names = ["Bob", 2, "Joe", "Lucy"];

    //Foreach loop parameters are swapped around.
    //Instead of a value from or in an array, it is each index of the array as a value.

    foreach ($names as $value) {
        echo "$value <br>";
    }

    echo "<br><br>";

    //The forloop uses the count function
    //Unlike other languages, this is a fucntion - not a property

    for ($i = 0; $i < count($names); $i++)
    {
        echo $names[$i]."<br>";
    }

    echo "<br><br>";

?>