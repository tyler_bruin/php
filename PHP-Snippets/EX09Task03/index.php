<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Embedding PHP in HTML</title>
        <link rel="stylesheet" href="" type="text/css" >
    </head>
    <body>
        
      <?php

        $myname = "John Doe";
        $age = 33;

        echo "Hi my name is $myname and I am $age years old.";

        ?>

    </body>
</html>