<?php

$date1 = date_create('1982-10-23');
$date2 = date_create('now');

$interval = date_diff($date1, $date2);
echo "I am ".$interval->format('%a')." days old";

echo "<br><br>";

$interval = date_diff($date1, $date2);
echo "I am ".$interval->format('%Y')." years old";

?>