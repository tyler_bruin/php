<?php

    echo "<h1>We do not want to do this! - learn to use functions</h1>";

    function getProduct($number1, $number2) {
        //inside a function we can put anything we want
        return $number1 * $number2;
    }

    function getSum($number1, $number2) {
        //inside a function we can put anything we want
        echo $number1 + $number2;
    }

?>