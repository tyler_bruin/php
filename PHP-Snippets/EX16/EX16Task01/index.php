<?php include('functions.php'); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Including PHP Functions</title>
        <link rel="stylesheet" href="main.css" type="text/css" >
    </head>
    <body>

        <h1>Here is a php function using echo() </h1>    
        <h2><?php echo getProduct(25, 100); ?></h2>

        <div>
            <p>Note we do not have a</p><pre><code>&lt;br&gt;</code></pre><p>tags in our php code </p>
        </div>

        <h1>Here is a php function which has an echo() built in </h1>    
        <h2>
            <?php getSum(25, 100); ?>
        </h2>
    
    </body>
</html>