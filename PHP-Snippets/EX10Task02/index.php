<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Embedding PHP in HTML</title>
        <link rel="stylesheet" href="" type="text/css" >
    </head>
    <body>
        
    <?php

        define("NAME", "John Doe");
        define("DOB", "1956/12/24");

        echo NAME." is born on ".DOB;

    ?>


    </body>
</html>